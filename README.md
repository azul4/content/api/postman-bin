# postman-bin

Build, test, and document your APIs faster

https://www.getpostman.com

Downloads:

https://www.postman.com/downloads/

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/api/postman-bin.git
```
